#include <iostream>

int main()
{
    std::cout << "Hello Skillbox!\n";

    const int N = 5;
    int array[N][N];

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = (i + j);
        }
    }

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            std::cout << array[i][j];
        }
        std::cout << "\n";
    }

    int monthstartcount = 25 % 5;
    int sum = 0;

    for (int j = 0; j < N; j++)
    {
        sum += array[monthstartcount][j]; 
    }
    std::cout << sum << std::endl;


}